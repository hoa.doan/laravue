/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

window.VueRouter = require('vue-router').default;

window.VueAxios=require('vue-axios').default;

window.Axios=require('axios').default;

let AppLayout= require('./components/App.vue');

Vue.component('navbar', require('./components/Navbar.vue'));

const articles = Vue.component('articles', require('./components/Articles.vue'));
const addArticle = Vue.component('articles', require('./components/AddArticle.vue'));
const editArticle = Vue.component('articles', require('./components/EditArticle.vue'));
const viewArticle = Vue.component('articles', require('./components/ViewArticle.vue'));

Vue.use(VueRouter,VueAxios, axios);

const routes = [
    {
        name: 'articles',
        path: '/',
        component: articles
    },
    {
        name: 'addArticle',
        path: '/add-article',
        component: addArticle
    },
    {
        name: 'editArticle',
        path: '/edit/:id',
        component: editArticle
    },
    {
        name: 'viewArticle',
        path: '/view/:id',
        component: viewArticle
    }
];

const router = new VueRouter({ mode: 'history', routes: routes});

new Vue(
    Vue.util.extend(
        { router },
        AppLayout
    )
).$mount('#app');
// Vue.component('articles', require('./components/Articles.vue'));
// Vue.component('example', require('./components/ExampleComponent.vue'));
//
// const app = new Vue({
//     el: '#app'
// });
